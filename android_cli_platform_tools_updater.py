#!/usr/bin/env python3


import os
import sys
import requests
import zipfile


BINARIES_WE_WANT = ["adb", "fastboot"]
INSTALL_LOCATION = "/opt"
BIN_LOCATION = "/opt/bin"
LATEST_RELEASE_URL = "https://dl.google.com/android/repository/platform-tools-latest-linux.zip"
TMP_FILE = "/tmp/platform-tools-latest-linux.zip"


def main():
    if os.geteuid() != 0:
        print("This script needs to be run with sudo or as root.", file=sys.stderr)
        exit(1)

    r = requests.get(LATEST_RELEASE_URL)
    open(TMP_FILE, "wb").write(r.content)

    extract_files(TMP_FILE, INSTALL_LOCATION)
    link_binaries(BINARIES_WE_WANT, BIN_LOCATION, INSTALL_LOCATION)
    os.remove(TMP_FILE)


def extract_files(tmp_file, install_location):
    with zipfile.ZipFile(tmp_file, "r") as zf:
        for folder in zf.namelist():
            if folder.startswith("platform-tools"):
                zf.extract(folder, install_location)


def link_binaries(binaries, bin_location, install_location):
    for binary in binaries:
        os.chmod("{}/platform-tools/{}".format(install_location, binary), 0o755)
        try:
            os.symlink("{}/platform-tools/{}".format(install_location, binary), "{}/{}".format(bin_location, binary))
        except FileExistsError:
            continue
            

main()
