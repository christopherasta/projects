#!/usr/bin/env python3

"""
csv_reader.py
A cli program to display .csv files nicely.
By: chris@asta.dev
"""

###################################################################################################

import argparse
import csv
import libasta

###################################################################################################

def main():

    """
    Function    : main
    Description : Programs entry point.
    """

    settings = set_arguments()
    list_of_lists = csv_to_lists(settings["file"])

    if settings["output_format"] in ["grid"]:
        libasta.print_aligned(list_of_lists, \
                           include_line_numbers=settings["include_line_numbers"], \
                           title_underline=settings["title_underline"])

    if settings["output_format"] in ["lines"]:
        print_lines(list_of_lists, settings)

    # Line space before the end of the program for looks.
    print("")

###################################################################################################

def set_arguments():

    """
    Function    : set_argv
    Description : Sets arguments.
    """

    parser = argparse.ArgumentParser(description="A cli program to display .csv files nicely.\n" + \
                                                 "By: chris@asta.dev\n", \
                                                 formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('file', type=argparse.FileType('r'), help="csv file to be displayed")

    parser.add_argument("-g", "--grid", action="store_true", \
                        help="display csv as a grid")

    parser.add_argument("-l", "--line", action="store_true", \
                        help="display each line individually")

    parser.add_argument("-nn", "--no_line_numbers", action="store_true", \
                        help="removes the line number from the output")

    parser.add_argument("-nu", "--no_title_underline", action="store_true", \
                        help="removes title underline")

    args = parser.parse_args()

    settings = {}

    settings["file"] = args.file.name

    if args.no_title_underline:
        settings["title_underline"] = "none"
    else:
        settings["title_underline"] = "column"

    # line_numbers block
    if args.no_line_numbers:
        settings["include_line_numbers"] = False
    else:
        settings["include_line_numbers"] = True

    # output_format block
    if args.grid and args.line:
        error_text = "Must select either grid or lines as the output format."
        invalid_argument(error_text, parser)
    elif args.line and not args.grid:
        settings["output_format"] = "lines"
    else:
        settings["output_format"] = "grid"

    return settings

###################################################################################################

def invalid_argument(error_text, parser):

    """
    Function    : invalid_argument
    Description : Displays the argument error and help text.
    """

    print("Error: %s" % (error_text))
    print("")
    parser.print_help()
    exit()

###################################################################################################

def csv_to_lists(file):

    """
    Function    : csv_to_lists
    Description : Takes a csv and converts it to a list of lists.
    """

    list_of_lists = []

    with open(file, "r") as csvfile:
        open_file = csv.reader(csvfile, delimiter=',')
        for row in open_file:
            list_of_lists.append(row)

    csvfile.close()
    return list_of_lists

###################################################################################################

def print_lines(list_of_lists, settings):

    """
    Function    : print_lines
    Description : Takes a list of lists and prints per line.
    """

    settings["title_type"] = "none"

    # Assume the first line is the title row.
    line_number = 1
    for i in list_of_lists[1:]:

        current_line = []
        current_index = 0

        for line in i:
            current_line.append([list_of_lists[0][current_index], line])
            current_index += 1

        if settings["include_line_numbers"] is True:
            current_line.insert(0, ["Line Number", line_number])

        libasta.print_aligned(current_line, \
                           include_line_numbers=settings["include_line_numbers"], \
                           title_underline="none")
        line_number += 1

###################################################################################################

main()
