#!/usr/bin/env python3


import os
import shutil
import sys
import requests
import tarfile


INSTALL_LOCATION = "/opt"
BIN_LOCATION = "/opt/bin"
LATEST_RELEASE_URL = "https://discord.com/api/download/stable?platform=linux&format=tar.gz"
TMP_FILE = "/tmp/discord-latest-linux.tar.gz"


def main():
    if os.geteuid() != 0:
        print("This script needs to be run with sudo or as root.", file=sys.stderr)
        exit(1)

    reinstall = check_install_location("{}/discord".format(INSTALL_LOCATION))
    
    r = requests.get(LATEST_RELEASE_URL)
    open(TMP_FILE, "wb").write(r.content)
    
    try:
        extract_files(INSTALL_LOCATION, TMP_FILE)
        if os.path.isdir("{}/discord".format(INSTALL_LOCATION)): 
            shutil.rmtree("{}/discord".format(INSTALL_LOCATION))
        os.rename("{}/Discord".format(INSTALL_LOCATION), "{}/discord".format(INSTALL_LOCATION))
        os.remove(TMP_FILE)
    except:
        print("Extraction failed.", file=sys.stderr)
        os.remove(TMP_FILE)
        exit(1) 
    
    os.chmod("{}/discord/Discord".format(INSTALL_LOCATION), 0o755)
    try:
        os.symlink("{}/discord/Discord".format(INSTALL_LOCATION), "{}/discord".format(BIN_LOCATION))
    except FileExistsError:
        pass
    
    print("Installation succeeded!")

def extract_files(install_location, tmp_file):
    tr = tarfile.open(tmp_file)
    tr.extractall(install_location)
    tr.close


def check_install_location(install_location):
    reinstall = False
    if not os.path.exists(install_location):
        os.mkdir(install_location)
    elif os.path.exists("{}".format(install_location)):
        print("{0} already exists.".format(install_location))
        response = input("Reinstall(Y/N)? ")
        if response not in ["Y", "y", "YES", "yes"]:
            exit(0)
        else:
            reinstall = True

    return reinstall
        
        
main()
