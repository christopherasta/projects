#!/usr/bin/env python3

"""
cli_guessing_game.py
A CLI python3 guessing game!
By: chris@asta.dev
"""

###################################################################################################

import libasta
import random
import time

###################################################################################################

def about_screen():

    print("============== About Guessing Game ==============")
    print("")
    print(" A python3 guessing game! You have three")
    print(" chances to guess a number between 1 and 10.")
    print("")
    print(" By: chris@asta.dev")
    print("")
    print("=================================================")

###################################################################################################

def main():

    """
    Function    : main
    Description : Programs entry point. Prints the main menu.
    """

    global WINS
    global STREAK

    # Clear screen and print menu.
    libasta.clear_terminal()

    print("======== The Guessing Game ========")
    print("")
    print(" Start     | 1 |    " + "Total Wins: " + str(WINS))
    print(" About     | 2 |    " + "Win Streak: " + str(STREAK))
    print(" Exit      | x | ")
    print("")
    print("===================================")
    print("")

    # Determine player's input and execute selection.
    selection = input("Please selection an option: ")

    if selection == "1":
        start_game()

    elif selection == "2":
        libasta.clear_terminal()
        about_screen()

    elif selection in ["x", "X", "exit", "EXIT"]:
        libasta.clear_terminal()
        exit()

    else:
        print("")
        print("Valid options are 1, 2, and x")

    continue_prompt()

###################################################################################################

def continue_prompt():

    """
    Function    : continue_prompt
    Description : Pauses for user to press enter. Returns to main menu.
    """

    print("")
    input("Press enter to continue.")
    main()

###################################################################################################

def start_game():

    """
    Function    : start_game
    Description : Starts the guessing game.
    """

    global WINS
    global STREAK

    # Set number of guesses and generate number.
    num_guesses = 3
    num = random.randint(1, 10)
    previous_guesses = []

    # Continue playing while the player still has at least one guess.
    while num_guesses > 0:

        libasta.clear_terminal()
        print("Guess a number 1 through 10!")

        # Print previous guesses if there are any.
        if previous_guesses:
            print("Previous Guesses: " + str(previous_guesses)[1:-1])

        else:
            print("Previous Guesses: None")

        print("Guesses Left: " + str(num_guesses))
        print("")
        guess = input("Guess (x to exit): ")

        # Return to main menu.
        if guess in ["x", "X", "exit", "EXIT"]:
            main()

        # Check to make sure the input was valid.
        elif validate_guess(guess) is False:
            print("")
            print("Invalid input. Please enter an integer between 1 and 10.")
            print("")
            input("Press enter to continue.")

        # Check to see if they already guessed that number.
        elif int(guess) in previous_guesses:
            print("")
            print("You already guessed " + str(guess) + ".")
            print("")
            input("Press enter to continue.")

        else:
            print("")
            # Initial progress bar print.
            libasta.print_progress_bar(0, 100, prefix="Thinking:", suffix="Complete", \
                                            bar_length=50)

            # Update Progress Bar
            for i in range(1, 100, 10):
                time.sleep(0.1)
                libasta.print_progress_bar(i+9, 100, prefix="Thinking:", \
                                                suffix="Complete", bar_length=50)

            # User wins if their guess equals the number. Send the user back to the main menu.
            if int(guess) == num:
                print("")
                print("Thats right! The number was " + str(num) + "! You win!")
                WINS += 1
                STREAK += 1
                continue_prompt()

            # Continue game if the guess was incorrect.
            else:
                print("")
                input(str(guess) + " is not the number. Press enter to continue.")
                previous_guesses.append(int(guess))
                num_guesses -= 1

    # If the user runs out of guesses they lose.
    libasta.clear_terminal()
    STREAK = 0
    print("You're out of guesses! You lose!")
    print("The number was " + str(num) + ".")
    # Head back to main menu.
    continue_prompt()

###################################################################################################

def validate_guess(guess):

    """
    Function    : validate_guess
    Description : Makes sure the players guess is an integer between 1 and 10
    """

    try:
        guess = int(guess)
        # Check that the guess is between 1 and 10.
        if guess < 1 or guess > 10:
            return False

    # Catches strings and other inputs during the int(guess) call.
    except Exception:
        return False

    return True

###################################################################################################

WINS = 0
STREAK = 0

main()
