"""
libasta.py
A library for some reusable python3 functions.
By: chris@asta.dev
"""

###################################################################################################

import hashlib
import os
import subprocess
import sys

###################################################################################################

def check_hash(password, stored_hash):

    password_hash = hash_password(password)
    if (stored_hash != password_hash):
        return False
    else:
        return True
        
###################################################################################################

def clear_terminal():

    """
    Function    : clear_terminal
    Description : Checks the OS and clears the terminal screen.
    """

    # Check to see if the system is running Windows.
    if os.name == "nt":
        subprocess.call("cls")

    # Else use the Unix version.
    else:
        subprocess.call("clear")

###################################################################################################

def create_hash_file(password, hash_file_path):
    password_hash = hashlib.blake2b()
    password_hash.update(password.encode(encoding = 'UTF-8', errors = 'strict'))
    hash_file = open(hash_file_path, "w")
    hash_file.write(password_hash.hexdigest())
    hash_file.close()
    os.chmod(hash_file_path, 0o600)

###################################################################################################

def hash_password(password):
    password_hash = hashlib.blake2b()
    password_hash.update(password.encode(encoding = 'UTF-8', errors = 'strict'))
    return password_hash.hexdigest()

###################################################################################################

def print_aligned(list_of_lists, alignment="left", buffer_length=2, include_line_numbers=False, title_underline="column"):

    """
    Function    : print_aligned
    Description : Takes a list of lists and prints them as columns with proper alignment.
    """

    if include_line_numbers:
        line_number = 0
        for i in list_of_lists:
            i.insert(0, line_number)
            line_number += 1

    # Find the widths of the items in the fist list to fill the dictionary.
    column_widths = {}
    increment = 0
    for i in list_of_lists[0]:
        i = str(i)
        column_widths[increment] = len(i)
        increment += 1

    # Check the rest of lists to see if they are wider than the first.
    for i in list_of_lists:

        increment = 0
        for j in i:
            j = str(j)

            if column_widths[increment] < len(j):
                column_widths[increment] = len(j)

            increment += 1

    # Calculate the total width of the columns
    increment = 0
    total_length = 0
    while increment < len(column_widths):
        total_length += column_widths[increment]
        total_length += buffer_length
        increment += 1

    # Subtract one buffer off the end since it get trimmed.
    header = "-" * (total_length - buffer_length)

    # Increment over the list, add the appropriate alignment/buffer/header, and output.
    print("")
    line_number = 0
    for i in list_of_lists:

        output = ""

        # Add the correct spacing between columns + alignment.
        increment = 0
        for j in i:
            j = str(j)

            remaining_width = (column_widths[increment] - len(j))
            remaining_buffer = " " * remaining_width

            if alignment == "left":
                output += j
                output += remaining_buffer

            elif alignment == "right":
                output += remaining_buffer
                output += j

            # Add in the buffer.
            output += " " * buffer_length
            increment += 1

        # Print the title underlines.
        if line_number == 1 and title_underline == "single":
            print(header)

        elif (line_number == 0 or line_number == 1) and title_underline == "double":
            print(header)

        elif line_number == 1 and title_underline == "column":
            header = ""
            increment = 0
            while increment < len(column_widths):
                header += "-" * column_widths[increment]
                header += " " * buffer_length
                increment += 1

            print(header)

        # Output each line.
        print(output.rstrip())
        line_number += 1

###################################################################################################

def print_progress_bar(iteration, total, prefix="", suffix="", pre_fill="-", fill_symbol="#", \
                       decimals=0, bar_length=100):

    """
    Function    : progress_bar
    Description : Prints a progress bar.
    """

    # Format bar type.
    string_format = "{0:." + str(decimals) + "f}"
    percents = string_format.format(100 * (iteration / float(total)))

    # Constructs how much of the bar is filled in.
    filled_length = int(round(bar_length * iteration / float(total)))
    bar_type = fill_symbol * filled_length + pre_fill * (bar_length - filled_length)

    # Prints the bar.
    print("\r%s [%s] %s%s %s" % (prefix, bar_type, percents, "%", suffix), end="")

    if iteration == total:
        print("")

###################################################################################################
