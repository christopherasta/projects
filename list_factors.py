#!/usr/bin/env python3

"""
list_factors.py
A cli program to find the factors of a number.
By: chris@asta.dev
"""

"""
TODO:
Add actual factoring tricks to improve performance.
"""

###################################################################################################

import argparse
import libasta
import os
import sys

###################################################################################################

def help_screen():

    """
    Function    : help_screen
    Description : Prints the help screen.
    """

    print("-------------------------- List Factors Help --------------------------")
    print("A python 3 program to find the factors of a number.")
    print("By: chris@asta.dev")
    print("")
    print("When a valid number is an argument the program will run without a menu.")
    print("")
    print("The large number warning is given for numbers above 100000000.")

###################################################################################################

def main():

    settings = set_arguments()

    if settings["use_menu"]:
        main_menu(settings)

    else:
        run_the_number(settings)

###################################################################################################

def main_menu(settings):

    """
    Function    : main_menu
    Description : Prints the main menu and handles user input.
    """

    libasta.clear_terminal()
    exit_settings = ["x", "X", "exit", "EXIT"]

    print("============= List Factors =============")
    print("")
    print(" Start                     | 1 |")
    print(" Output to Screen          | 2 | [" + str(settings["put_screen"]) + "]")
    print(" Output to file            | 3 | [" + str(settings["put_file"]) + "]")
    print(" Ignore Large Number Error | 4 | [" + str(settings["force"]) + "]")
    print(" Help                      | 5 |")
    print(" Exit                      | x |")
    print("")
    print("========================================")

    print("")
    selection = input("Please select an option: ")

    # Asks for a number and then finds it factors.
    if selection == "1":

        if settings["put_screen"] is False and settings["put_file"] is False:
            print("")
            print("Please select either to output to the screen or to a file.")
            continue_prompt(settings)

        print("")
        settings["number"] = input("Please enter a positive integer (x to exit): ")

        if settings["number"] in exit_settings:
            main_menu(settings)

        if validate_number(settings) is False:
            print("")
            print("Invalid Input. Please input a positive integer.")

        else:
            run_the_number(settings)

    elif selection == "2":
        settings["put_screen"] = not settings["put_screen"]
        main_menu(settings)

    elif selection == "3":
        settings["put_file"] = not settings["put_file"]
        main_menu(settings)

    elif selection == "4":
        settings["force"] = not settings["force"]
        main_menu(settings)

    elif selection == "5":
        libasta.clear_terminal()
        help_screen()

    elif selection in exit_settings:
        libasta.clear_terminal()
        exit()

    else:
        print("")
        print("Invalid selection. Valid settings are 1, 2, 3, 4, 5, and x")

    continue_prompt(settings)

###################################################################################################

def continue_prompt(settings):

    """
    Function    : continue_prompt
    Description : Waits for user to press Enter. Resets back to main menu.
    """
    print("")
    input("Press enter to continue.")
    main_menu(settings)

###################################################################################################

def set_arguments():

    """
    Function    : set_arguments
    Description : Parse arguments and update settings.
    """

    # Default settings.
    settings = {"number":1,\
             "put_screen":True,\
             "put_file":False,\
             "force":False,\
             "use_menu":True}

    # If there are arguments present set them. Else return settings without change.
    if len(sys.argv) > 1:
        settings["use_menu"] = False
        parser = argparse.ArgumentParser( \
                          description="A cli program to find the factors of a number.\n",
                          formatter_class=argparse.RawTextHelpFormatter)

        parser.add_argument("number",
                            type=int,
                            help="a positive integer")

        parser.add_argument("-f", "--force",
                            action="store_true",
                            help="ignore large number warning")
        args = parser.parse_args()

        if args.number < 1:
            print("")
            print("The number needs to be positive.")
            print("Run without any arguments for a menu driven version.")
            print("")
            exit()

        settings["number"] = args.number

        if args.force:
            settings["force"] = True

    return settings

#######################################################################

def validate_number(settings):

    """
    Function    : validate_number
    Description : Checks the provided number is a positive integer.
    """

    # Check to make sure the number is a postive integer.
    try:
        settings["number"] = int(settings["number"])
        if settings["number"] < 1:
            return False

    except Exception:
        return False

    return True

###################################################################################################

def run_the_number(settings):

    """
    Function    : run_the_number
    Description : Runs the number based on the settings.
    """

    # Check for large number and force flag.
    if settings["force"] is False and settings["number"] > 100000000:
        space_heater(settings)
        continue_prompt(settings)

    # Generate factor list.
    factor_list = find_factors(settings["number"])

    if settings["put_file"] is True:
        make_file(factor_list)

    if settings["put_screen"] is True:
        print_factors(factor_list, settings)

###################################################################################################

def space_heater(settings):

    """
    Function    : space_heater
    Description : Exits if the provided number is greater than 100000000.
    """

    print("")
    print(str(settings["number"]) + " is greater than the limit of 100000000.")
    print("")
    print("This program does not use any fancy factoring tricks so it can be")
    print("VERY CPU intensive. Setting \"Ignore Large Number Error\" to True")
    print("or using the --force flag will bypass this error.")

    # Exit if run straight from the command line.
    if settings["use_menu"] is False:
        print("")
        exit()

###################################################################################################

def find_factors(number):

    """
    Function    : find_factors
    Description : Takes a number, finds it factors, and puts them in a list.
    """

    factor_list = []

    # This is pretty inefficient, but it is consistent.
    for i in range(1, int((number+1)/2)):
        if number % i == 0:
            factor_list.append(i)
            factor_list.append(int(number/i))

    factor_list = list(set(factor_list))
    factor_list.sort()

    return factor_list

###################################################################################################

def print_factors(factor_list, settings):

    """
    Function    : print_factors
    Description : Prints the list of factor list to the screen.
    """
    
    # Fancy text for menu version.
    if settings["use_menu"] is True:
        print("")
        print("The factors of " + str(settings["number"]) + " are:")

    # Print each factor on a new line.
    for i in factor_list:
        print(i)

    # Check if its a prime number for the menu version.
    if settings["use_menu"] is True and len(factor_list) == 2:
        print("")
        print(str(settings["number"]) + " is a prime number!")

###################################################################################################

def make_file(factor_list):

    """
    Function    : make_file
    Description : Creates a file with the factors of the number.
    """

    print("")
    filename = input("File name: ")

    # Tries to create a file in the current directory.
    try:
        with open(filename, "w") as file:
            for i in factor_list:
                file.write(str(i) + "\n")

            file.close()

        print("")
        print("File Location: " + os.getcwd())

    # Catches invlid files names and other stuff.
    except Exception:
        print("")
        print("Error in file creation. Likely invalid filename.")

###################################################################################################

main()
