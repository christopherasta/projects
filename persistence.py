#!/usr/bin/env python3

"""
persistence.py
A python3 program to find the persistence of a number.
By: chris@asta.dev
"""

###################################################################################################

import libasta

###################################################################################################

def about_screen():

    """
    Function    : about_screen
    Description : Prints the about screen.
    """

    print("---------------------- Persistence ----------------------")
    print("")
    print("A python 3 program to find the persistence of a number.")
    print("By: chris@asta.dev")
    print("")
    print("Credit to Brady Haren of numberphile")
    print("and Matt Parker of standupmaths for the idea.")
    print("")
    print("See https://en.wikipedia.org/wiki/Persistence_of_a_number")
    print("for more info on the persistence of a number.")


###################################################################################################

def main(number):

    """
    Function    : main
    Description : Programs stating point. Prints main menu.
    """

    # Clear screen and print main menu.
    libasta.clear_terminal()
    print("=========== Persistence ===========")
    print("")
    print(" Current Number: [" + str(number) + "]")
    print("")
    print(" 1. Additive Persistence")
    print(" 2. Multiplicative Persistence")
    print(" 3. Change Number")
    print(" 4. Info")
    print(" x. Exit")
    print("")
    print("===================================")
    print("")

    # Get and execute user selection
    selection = input("Please select an option: ")

    if selection == "1":
        print("")
        print("Starting number: " + str(number))
        find_additive_persistence(number)

    elif selection == "2":
        print("")
        print("Starting number: " + str(number))
        find_multiplicative_persistence(number)

    elif selection == "3":
        validate_number(number)

    elif selection == "4":
        libasta.clear_terminal()
        about_screen()

    elif selection in ["x", "X", "exit", "EXIT"]:
        libasta.clear_terminal()
        exit()

    else:
        print("")
        print("Valid options are 1, 2, 3, 4, and x")

    # Reload main menu if exit wasn't selected.
    continue_prompt(number)

###################################################################################################

def continue_prompt(number):

    """
    Function    : continue_prompt
    Description : Waits for user to press Enter then reloads the main menu.
    """

    print("")
    input("Press enter to continue.")
    main(number)

###################################################################################################

def validate_number(number):

    """
    Function    : validate_number
    Description : Asks the user for a number and validates it.
    """

    print("")
    new_number = input("Please enter a postitive integer: ")
    invalid_msg = "Only positive integers have persistance."

    try:

        new_number = int(new_number)
        
        # Make sure the number is positive.
        if new_number < 1:
            print("")
            print(invalid_msg)
            continue_prompt(number)

    # Catches inputs such as strings on the int(new_number) call.
    except Exception:

        print("")
        print(invalid_msg)
        continue_prompt(number)

    main(new_number)

###################################################################################################

def find_additive_persistence(number, persistence=0):

    """
    Function    : find_additive_persistence
    Description : Prints the additive persistance.
    """

    # Check if the number is only 1 digit long to end loop.
    if len(str(number)) == 1:
        print("")
        print("Additive Persistence: " + str(persistence))
        return

    persistence += 1
    result = 0
    # Split number up into its digits.
    digits = [int(i) for i in str(number)]
    for j in digits:
       result += j

    # Prints and reruns loop with the result.
    print(str(persistence) + " : "+ str(result))
    find_additive_persistence(result, persistence)

###################################################################################################

def find_multiplicative_persistence(number, persistence=0):

    """
    Function    : find_multiplicative_persistence
    Description : Prints the additive persistance.
    """

    # Check if the number is only 1 digit long to end loop.
    if len(str(number)) == 1:
        print("")
        print("Multiplicitive Persistence: " + str(persistence))
        return

    persistence += 1
    result = 1
    # Split number up into its digits.
    digits = [int(i) for i in str(number)]
    for j in digits:
        result *= j

    # Prints and reruns loop with the result.
    print(str(persistence).zfill(2) + " : " + str(result))
    find_multiplicative_persistence(result, persistence)

###################################################################################################

# Start program with the a number.
main(277777788888899)
