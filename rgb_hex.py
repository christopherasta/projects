#!/usr/bin/python3

"""
rgb_hex.py
A cli program to convert between HEX and RGB.
By: chris@asta.dev
"""

###################################################################################################

import libasta

###################################################################################################

def continue_prompt():

    """
    Function    : continue_prompt
    Description : Pauses for user to press enter. Returns to main menu.
    """

    print("")
    input("Press enter to continue.")
    main()

###################################################################################################

def rgb_hex():

    """
    Function    : rgb_hex
    Description : Convert from RGB to Hex and output to screen.
    """

    libasta.clear_terminal()
    invalid_msg = "Invalid input. RGB values are integers between 0 and 255."

    # Try loop to catch invalid inputs such as strings.
    try:
        red = int(input("Red (R) value   : "))
        if (red < 0 or red > 255):
            print("")
            print(invalid_msg)
            continue_prompt()

        green = int(input("Green (G) value : "))
        if (green < 0 or green > 255):
            print("")
            print(invalid_msg)
            continue_prompt()

        blue = int(input("Blue (B) value  : "))
        if (blue < 0 or blue > 255):
            print("")
            print(invalid_msg)
            continue_prompt()

        val = (red << 16) + (green << 8) + blue

        # Clear screen and print conversion.
        libasta.clear_terminal()
        print("Red   (R): " + str(red).zfill(3))
        print("Green (G): " + str(green).zfill(3))
        print("Blue  (B): " + str(blue).zfill(3))
        print("")
        print("Hex : #" + str((hex(val)[2:]).upper()).zfill(6))

        continue_prompt()

    except Exception:
        print("")
        print(invalid_msg)
        continue_prompt()

###################################################################################################

def hex_rgb():

    """
    Function    : hex_rgb
    Description : Convert from Hex to RGB and output to screen.
    """

    libasta.clear_terminal()

    # Main loop to catch invalid inputs.
    try:
        hex_input = input("Hex value: #")

        if len(hex_input) != 6:
            print("")
            print("Hex values are six alphanumeric values long.")
            continue_prompt()

        else:
            hex_val = int(hex_input, 16)

        # Convert the Hex to rgb
        two_hex_digits = 2 ** 8

        blue = hex_val % two_hex_digits
        hex_val = hex_val >> 8

        green = hex_val % two_hex_digits
        hex_val = hex_val >> 8

        red = hex_val % two_hex_digits

        # Clear screen and print conversion.
        libasta.clear_terminal()
        print("Hex : #" + str(hex_input).zfill(6))
        print("")
        print("Red   (R): " + str(red).zfill(3))
        print("Green (G): " + str(green).zfill(3))
        print("Blue  (B): " + str(blue).zfill(3))

        continue_prompt()

    except Exception:
        print("")
        print("Invalid Hex value.")
        continue_prompt()

###################################################################################################

def main():

    """
    Function    : main
    Description : Main proc to start program.
    """

    # Clear screen and print the menu.
    libasta.clear_terminal()
    print("RGB <-> Hex Converter")
    print("")
    print("RGB to Hex : 1")
    print("Hex to RGB : 2")
    print("Exit       : x")
    print("")

    # Get and run user selected option.
    option = input("Please select an option: ")

    if option == "1":
        rgb_hex()

    elif option == "2":
        hex_rgb()

    elif option in ("x", "X", "exit", "EXIT"):
        libasta.clear_terminal()
        exit()

    else:
        print("")
        print("Invalid option. Valid options are 1, 2 , or x.")
        continue_prompt()

###################################################################################################

main()
