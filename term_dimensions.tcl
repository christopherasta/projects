#!/usr/bin/tclsh

# term_dimension.tcl
# Outputs current terminal dimensions.
# By: chris@asta.dev

proc Main { } {

    set term_dimensions [GetTermDimensions]

    puts "Height: [lindex $term_dimensions 0]"
    puts "Width: [lindex $term_dimensions 1]"

}

proc GetTermDimensions { } {

    set stty_size [exec stty size]
    set stty_size [split $stty_size " "]
    return [list [lindex $stty_size 0] [lindex $stty_size 1]]

}

Main
