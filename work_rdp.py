#!/usr/bin/env python3


import argparse
import libasta
import json
import os
import subprocess
import tkinter as tk
from tkinter import messagebox


class Work_RDP():
    def __init__(self):
        pass
        
    def main_window(self, root):
        root.title("Work RDP")
        root.geometry("595x303")
        args = get_arguments()
        if os.path.exists(args.config_file):
            try:
                self.configuration = read_json_file(args.config_file)
            except:
                messagebox.showwarning("","Failed to load {}".format(str(args.config_file)))
                exit(1)
        else:
            self.configuration = default_configuration_file(args.config_file)
            messagebox.showinfo("No configuration file found.", "Wrote defaults to {}".format(str(args.config_file)))

        label = tk.Label(root, text="FreeRDP Settings", pady=10)
        label.grid(row=0, columnspan=2, sticky="n")
    
        configuration_file_path_box_label = tk.Label(root, text="Config File Path")
        configuration_file_path_box = tk.Entry(root, width=45)
        configuration_file_path_box.insert(0, args.config_file)
        configuration_file_path_box_label.grid(row=1, column=0, sticky="e", padx=5)
        configuration_file_path_box.grid(row=1, column=1, sticky="w", padx=5)
        
        gateway_hostname_box_label = tk.Label(root, text="Gateway Hostname")
        gateway_hostname_box = tk.Entry(root, width=45)
        gateway_hostname_box.insert(0, self.configuration["gateway_hostname"])
        gateway_hostname_box_label.grid(row=2, column=0, sticky="e", padx=5)
        gateway_hostname_box.grid(row=2, column=1, sticky="w", padx=5)
        
        gateway_domain_box_label = tk.Label(root, text="Gateway Domain")
        gateway_domain_box = tk.Entry(root, width=45)
        gateway_domain_box.insert(0, self.configuration["gateway_domain"])
        gateway_domain_box_label.grid(row=3, column=0, sticky="e", padx=5)
        gateway_domain_box.grid(row=3, column=1, sticky="w", padx=5)
    
        server_hostname_box_label = tk.Label(root, text="Server Hostname")
        server_hostname_box = tk.Entry(root, width=45)
        server_hostname_box.insert(0, self.configuration["server_hostname"])
        server_hostname_box_label.grid(row=4, column=0, sticky="e", padx=5)
        server_hostname_box.grid(row=4, column=1, sticky="w", padx=5)
    
        rdp_settings_box_label = tk.Label(root, text="RDP Settings")
        rdp_settings_box = tk.Entry(root, width=45)
        rdp_settings_box.insert(0, self.configuration["rdp_settings"])
        rdp_settings_box_label.grid(row=5, column=0, sticky="e", padx=5)
        rdp_settings_box.grid(row=5, column=1, sticky="w", padx=5)
        
        monitor_settings_box_label = tk.Label(root, text="Monitor Settings")
        monitor_settings_box = tk.Entry(root, width=45)
        monitor_settings_box.insert(0, self.configuration["monitor_settings"])
        monitor_settings_box_label.grid(row=6, column=0, sticky="e", padx=5)
        monitor_settings_box.grid(row=6, column=1, sticky="w", padx=5)
    
        username_box_label = tk.Label(root, text="Username")
        username_box = tk.Entry(root, width=45)
        username_box.insert(0, self.configuration["username"])
        username_box_label.grid(row=7, column=0, sticky="e", padx=5)
        username_box.grid(row=7, column=1, sticky="w", padx=5)
    
        password_box_label = tk.Label(root, text="Password")
        password_box = tk.Entry(root, width=45, show="*")
        password_box.focus_set()
        password_box_label.grid(row=8, column=0, sticky="e", padx=5)
        password_box.grid(row=8, column=1, sticky="w", padx=5)
        
        debug_box_var = tk.BooleanVar()
        debug_box = tk.Checkbutton(root, variable=debug_box_var, text = "Debug")
        debug_box.grid(row=9, column=0, pady=5)
    
        check_password_var = tk.IntVar(value=1)
        check_password_box = tk.Checkbutton(root, variable=check_password_var, text = "Check Password", onvalue=1, offvalue=0)
        check_password_box.grid(row=9, column=1, sticky="w", padx=35, pady=5)

        display_server = tk.StringVar(value=self.configuration["display_server"])
        wl_button = tk.Radiobutton(root, text="Wayland", variable=display_server, indicatoron=False, value="wl", width=8)
        x_button = tk.Radiobutton(root, text="X.Org", variable=display_server, indicatoron=False, value="x", width=8)
        wl_button.grid(row=9, column=1, sticky="e", padx=76)
        x_button.grid(row=9, column=1, sticky="e", padx=5)

        entry_boxes = {"configuration_file_path_box": configuration_file_path_box,
                       "gateway_hostname_box": gateway_hostname_box,
                       "gateway_domain_box": gateway_domain_box,
                       "server_hostname_box": server_hostname_box,
                       "rdp_settings_box": rdp_settings_box,
                       "monitor_settings_box": monitor_settings_box,
                       "username_box": username_box,
                       "password_box": password_box,
                       "debug_box_var": debug_box_var,
                       "check_password_var": check_password_var,
                       "display_server": display_server
                      }
                 
        save_config_button = tk.Button(root, width=15, text="Save Configuration", command=lambda : save_configuration(entry_boxes))   
        save_config_button.grid(row=10, column=0, padx=5)
    
        set_hash_button = tk.Button(root, width=15, text="Set Password Hash", command=lambda : set_password_hash(password_box.get(), args.config_file))
        set_hash_button.grid(row=10, columnspan=2, padx=5)
        
        root.bind("<Return>", lambda event: connect(entry_boxes, self.configuration))
        connect_button = tk.Button(root, width=15, text="Connect", command=lambda : connect(entry_boxes, self.configuration))
        connect_button.grid(row=10, column=1, padx=5, sticky="e")


def save_configuration(entry_boxes):
    new_config = get_entries(entry_boxes)
    
    if not messagebox.askyesno("","Save current configuration to {}?".format(new_config["configuration_file_path_box"])):
        return

    current_config = read_json_file(new_config["configuration_file_path_box"])

    # Only write the settings we actually want to save.
    for i in ["gateway_hostname", "gateway_domain", "server_hostname", "rdp_settings", "monitor_settings", "username", "display_server"]:
        current_config[i] = new_config[i]
     
    write_json_file(new_config["configuration_file_path_box"], current_config)

def set_password_hash(password, config_file):
    current_config = read_json_file(config_file)
    if not messagebox.askyesno("","Save current password hash to {}?".format(config_file)):
        return
    password_hash = libasta.hash_password(password)
    current_config["password_hash"] = password_hash
    
    write_json_file(config_file, current_config)

def default_configuration_file(config_file):
    configuration = {}
    configuration["gateway_hostname"] = ""
    configuration["gateway_domain"] = ""
    configuration["server_hostname"] = ""
    configuration["username"] = ""
    configuration["rdp_settings"] = []
    configuration["monitor_settings"] = ["/f"]
    configuration["password_hash"] = ""
    configuration["display_server"] = "wl"
    write_json_file(config_file, configuration)
    return configuration
    
def read_json_file(file_path):
    with open(file_path, "r") as cf:
        json_dict = json.load(cf)
    return json_dict
    
def write_json_file(file_path, json_dict):
    with open(file_path, "w") as cf:
        json.dump(json_dict, cf)
    
    
def connect(entry_boxes, configuration):
    rdp_config = get_entries(entry_boxes)
    # Check if VPN is running
    if not check_for_vpn_route(rdp_config["gateway_hostname"]):
        return
    
    # Check the password hashes match
    if rdp_config["check_password"] == 1:
        if not libasta.check_hash(rdp_config["password"], configuration["password_hash"]):
            messagebox.showerror("Error", "Incorrect password provided!")
            return
        
    rdp_command = configure_rdp_command(rdp_config)
    if rdp_config["debug"]:
        rdp_command.insert(0, "kgx")

    rdp = subprocess.run(rdp_command, capture_output=(rdp_config["debug"]))
    
    
def get_entries(entry_boxes):
    rdp_config = {}
    rdp_config["configuration_file_path_box"] = entry_boxes["configuration_file_path_box"].get()
    rdp_config["gateway_hostname"] = entry_boxes["gateway_hostname_box"].get()
    rdp_config["gateway_domain"] = entry_boxes["gateway_domain_box"].get()
    rdp_config["server_hostname"] = entry_boxes["server_hostname_box"].get()
    rdp_config["rdp_settings"] = entry_boxes["rdp_settings_box"].get()
    rdp_config["monitor_settings"] = entry_boxes["monitor_settings_box"].get()
    rdp_config["username"] = entry_boxes["username_box"].get()
    rdp_config["password"] = entry_boxes["password_box"].get()
    rdp_config["debug"] = entry_boxes["debug_box_var"].get()
    rdp_config["check_password"] = entry_boxes["check_password_var"].get()
    rdp_config["display_server"] = entry_boxes["display_server"].get()
    return rdp_config


def check_for_vpn_route(gateway_hostname):
    routes = subprocess.run(["ip", "route"], capture_output=True, text=True)
    if gateway_hostname not in routes.stdout:
        messagebox.showerror("Error", "{} not found in ip route.\nIs pulse connected?".format(gateway_hostname))
        return False
    else:
        return True


def configure_rdp_command(rdp_config):
    rdp_command = ["{}freerdp".format(rdp_config["display_server"]),
                   "/g:{}".format(rdp_config["gateway_hostname"]),
                   "/gd:{}".format(rdp_config["gateway_domain"]),
                   "/v:{}".format(rdp_config["server_hostname"]),
                   "/u:{}".format(rdp_config["username"]),
                   "/gu:{}".format(rdp_config["username"]),
                   "/gp:{}".format(rdp_config["password"]),
                   "/p:{}".format(rdp_config["password"])
                  ]

    for setting in rdp_config["rdp_settings"].split(" "):
        if setting != "":
            rdp_command.append(setting)
            
    for setting in rdp_config["monitor_settings"].split(" "):
        if setting != "":
            rdp_command.append(setting)
            
    return rdp_command


def get_arguments():
    parser = argparse.ArgumentParser(description="A wrapper to easily rdp to my work computer.")
    parser.add_argument("-c", "--config-file", help="Path of the configuration file.", default="~/.work_rdp_conf.json")
    args = parser.parse_args()
    args.config_file = os.path.expanduser(args.config_file)
    return args
    
    
if __name__ == "__main__":
    root = tk.Tk()
    work_rdp = Work_RDP()
    work_rdp.main_window(root)
    root.mainloop()
