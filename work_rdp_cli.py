#!/usr/bin/env python3

"""
work_vpn.py
A cli program to start my work vpn.
By: Chris Asta (chris@asta.dev)
"""

"""
TODO
Add check for work_vpn
"""

################################################################################

import argparse
import getpass
import hashlib
import os
import subprocess

HASH_FILE = os.path.expanduser("~/.work_rdp.hash")

GATEWAY = "10.32.20.20"
GATEWAY_DOMAIN = "optiverus"
REMOTE_HOST = "opuscyenwc3298"
USERNAME = "christopherasta"

################################################################################

def main():

    settings = get_arguments()

    password = getpass.getpass("Password: ")

    if settings.create_hash is True:
        print("Creating hash file at " + str(HASH_FILE))
        create_hash_file(password)
        exit(0)
    
    check_hash(password)
    rdp_command = configure_rdp_command(settings.display_server, password)
    start_rdp(rdp_command, settings)
    
################################################################################

def get_arguments():
                 
    parser = argparse.ArgumentParser(description="A wrapper to easily rdp to my work computer.")
    parser.add_argument("-c", "--create-hash", action="store_true",
                        help="Even if there is a hash file, create a new one and exit.")
    parser.add_argument("-d", "--debug", action="store_true",
                        help="Instead of disowning the rdp process, display output so things can be debugged.")
    parser.add_argument("-ds", "--display_server",
                        choices = ["w", "wl", "wayland", "x", "xorg"], default="wl")
    args = parser.parse_args()
    
    if args.display_server in ["w", "wayland"]:
        args.display_server = "wl"
    elif args.display_server == "xorg":
        args.display_server == "x"
        
    return args
    
################################################################################

def check_hash(password):
    if not (os.path.exists(HASH_FILE)):
        create_hash_response = input("Hash file not found. Would you like to make one? ")
        if create_hash_response.lower() in ["y","yes"]:
            create_hash_file(password)
        else:
            print("No hash to reference. Exiting.")
            exit(1)

    stored_hash_file = open(HASH_FILE, "r")
    stored_hash = stored_hash_file.read()

    is_match = False
    while is_match is not True:
        password_hash = hashlib.blake2b()
        password_hash.update(password.encode(encoding = 'UTF-8', errors = 'strict'))
        if (stored_hash != password_hash.hexdigest()):
            print("Incorrect password provided.")
            password = getpass.getpass("Password: ")
        else:
            is_match = True
        
################################################################################

def create_hash_file(password):
    password_hash = hashlib.blake2b()
    password_hash.update(password.encode(encoding = 'UTF-8', errors = 'strict'))
    hash_file = open(HASH_FILE, "w")
    hash_file.write(password_hash.hexdigest())
    hash_file.close()
    os.chmod(HASH_FILE, 0o600)
    
################################################################################

def configure_rdp_command(display_server, password):
    rdp_command = [f"{display_server}freerdp",
                   f"/g:{GATEWAY}",
                   f"/gd:{GATEWAY_DOMAIN}",
                   f"/v:{REMOTE_HOST}",
                   f"/u:{USERNAME}",
                   f"/gu:{USERNAME}",
                   "/gp:" + password,
                   "/p:" + password,
                   "+auto-reconnect",
                   "/auto-reconnect-max-retries:2",
                   "/f",
                   "/sound",
                  ]

    return rdp_command

################################################################################

def start_rdp(rdp_command, settings):
    if settings.debug:
        for option in rdp_command:
            if "/p:" in option or "/gp:" in option:
                continue
            else:
                print(option)

    try:
        rdp = subprocess.run(rdp_command, capture_output=(not settings.debug))
                               
    except KeyboardInterrupt:
        print("Keyboard interrupt recieved. Exiting.")
        exit()
     
################################################################################

main()
